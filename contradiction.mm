$c True False -> wff , empty context |- ; $.

$v A B $.
wff-A $f wff A $.
wff-B $f wff B $.

wff-True  $a wff True $.
wff-False $a wff False $.
wff-imp   $a wff A -> B $.

$v G $.
context-G $f context G $.

context-concat $a context G , A $.
context-empty  $a context empty $.

ax-True $a |- G ; True $.

${ ax-False.1 $e |- G ; False $.
   ax-False   $a |- G ; A $.
$}

${ ax-mp.1 $e |- G ; A $.
   ax-mp.2 $e |- G ; A -> B $.
   ax-mp   $a |- G ; B $.
$}

${ ax-intro-imp.1 $e |- G , A ; B $.
   ax-intro-imp   $a |- G ; A -> B $.
$}

ax-context $a |- G , A ; A $.

${ ax-weakening.1 $e |- G ; B $.
   ax-weakening   $a |- G , A ; B $.
$}

identity $p |- G ; A -> A $=
  wff-A                      $( wff A        $)
  wff-A                      $( wff A        $)
  context-G                  $( context G    $)
  wff-A context-G ax-context $( |- G , A ; A $)
ax-intro-imp $.

contradiction $p |- G ; False $=
  wff-False wff-False wff-imp      $( wff False -> False $)
  wff-False                        $( wff False $)
  context-G                        $( context G $)
  wff-False context-G identity     $( |- G ; False -> False $)
    wff-False                        $( wff False $)
    wff-False wff-False wff-imp      $( wff False -> False $)
    context-G                        $( context G $)
      wff-False                        $( wff False $)
      wff-False wff-False wff-imp      $( wff False -> False $)
      context-G                        $( context G $) 
      wff-False context-G identity     $( |- G ; False -> False $)
    ax-weakening                     $( |- G , False ; False -> False $)
  ax-intro-imp                     $( |- G ; False -> False -> False $)
ax-mp $.
